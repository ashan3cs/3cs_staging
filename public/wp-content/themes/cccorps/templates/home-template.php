<?php
/**
 * Template Name: Home Page Template
 *
 *
 */
 get_header();
 ?>
 <?php
  get_template_part('template-parts/homepage-parts/topvideo-block');
  ?>
 <div class="bs-example">
    <div class="panel panel-default">
        <div class="panel-body">Home Page</div>
    </div>
</div>
  <?php
    get_template_part('template-parts/homepage-parts/home-body-tile');
   ?>
   <div class="">
     <h1></h1>
     <p></p>
   </div>
   <?php
    get_template_part('template-parts/homepage-parts/map');
    ?>
 <?php
 get_footer();
  ?>
